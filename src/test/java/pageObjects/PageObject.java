package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Factory Page to initialize all WebElements
 * @author MarcosVinícius
 *
 */
public class PageObject {
	
	/**
	 * Instantiate driver
	 */
	protected WebDriver driver;
	
	/**
	 * Constructor that initialize all pages
	 * @param driver
	 */
	public PageObject(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
