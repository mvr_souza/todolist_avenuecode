package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * PageObject of the LogIn Page
 * @author MarcosVinícius
 *
 */
public class LogInPage extends PageObject{
	
	/**
	 * WebElements
	 */
	@FindBy(id="user_email")
	private WebElement txt_username;
	
	@FindBy(id="user_password")
	private WebElement txt_password;
	
	@FindBy(name="commit")
	private WebElement btn_signIn;
	
	@FindBy(className="alert")
	private WebElement alert_message;
	
	/**
	 * Constructor
	 * @param driver
	 */
	public LogInPage (WebDriver driver) {
		super(driver);
	}
	
	/**
	 * Method to sign in ToDoList App
	 * @param user
	 * @param password
	 * @return TasksPage
	 */
	public TasksPage login(String user, String password) {
		
		this.txt_username.sendKeys(user);
		this.txt_password.sendKeys(password);
		this.btn_signIn.click();
		
		return new TasksPage(driver);
	}
	
	/***
	 * Method to validate if the page is loaded
	 * @return boolean
	 */
	public boolean isOpen() {
		
		return txt_username.isDisplayed();
	}
	
	/**
	 * Method to get the Error/Success Message
	 * @return String
	 */
	public String getAlertMessage() {
		
		return alert_message.getText();
	}
		
}
