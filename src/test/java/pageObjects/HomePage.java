package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * PageObject of the Index of the system
 * @author MarcosVinícius
 *
 */
public class HomePage extends PageObject{
	
	/**
	 * WebElements
	 */
	@FindBy(partialLinkText="Sign In")
	private WebElement link_Login;
	
	@FindBy(partialLinkText="My Tasks")
	private WebElement link_MyTasks;
	
	
	/**
	 * Constructor 
	 * @param driver
	 */
	public HomePage(WebDriver driver) {
		super(driver);
	}
	/***
	 * Method to find LogIn link to validate access to the system
	 * @param driver
	 * @return WebElement LogIn
	 */
	public LogInPage logIn() {
		
		this.link_Login.click();
		
		return new LogInPage(driver);
	}
	
	/***
	 * Method to validate if the page is loaded
	 * @param driver
	 * @return boolean
	 */
	public boolean isOpen() {
		
		return link_MyTasks.isDisplayed();
	}
	
	/**
	 * Method to get the Error/Success Message
	 * @return String
	 */
	public String getMyTasks() {
		
		return link_MyTasks.getText();
	}
}
