package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Page Object of Tasks Page
 * @author MarcosVinícius
 *
 */
public class TasksPage extends PageObject {
		
	/**
	 * WebElements
	 */
	@FindBy(className="alert-info")
	private WebDriver alert_success;
	
	@FindBy(tagName="h1")
	private WebElement label_userLogged;
	
	@FindBy(id="new_task")
	private WebElement new_task;
	
	@FindBy(className="input-group-addon")
	private WebElement btn_add_task;
	
	@FindBy(tagName="tbody")
	private WebElement table_todolist;
	
	@FindBy(partialLinkText="Manage Subtasks")
	private WebElement btn_add_subTask;
	
	@FindBy(id="new_sub_task")
	private WebElement name_new_subTask;
	
	@FindBy(className="editable-click")
	public WebElement task_saved;
	
	/**
	 * Constructor
	 * @param driver
	 */
	public TasksPage(WebDriver driver) {
		super(driver);
	}
	
	/***
	 * Method to validate if the page is loaded
	 * @param driver
	 * @return boolean
	 */
	public boolean isOpen() {
		
		return label_userLogged.isDisplayed();
	}

	/**
	 * Method to return wellcome text 
	 * @return
	 */
	public String getWellcomeText() {
		return label_userLogged.getText();
	}
	
	public String addNewTask(String taskName) {
		
		this.new_task.sendKeys(taskName);
		this.btn_add_task.click();
		
		return alert_success.getTitle();
	}
}
