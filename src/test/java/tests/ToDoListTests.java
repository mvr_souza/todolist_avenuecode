package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pageObjects.HomePage;
import pageObjects.LogInPage;
import pageObjects.TasksPage;

import static org.hamcrest.CoreMatchers.is;  
import static org.hamcrest.MatcherAssert.assertThat;  

import java.util.concurrent.TimeUnit;  

public class ToDoListTests {

	private static WebDriver driver = null;
	
	@Before
	public void SetUp(){
		
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\Marcos Vin�cius\\git\\workspace\\AvenueCodeTest\\Selenium\\geckodriver.exe");
		
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		
		driver.get("http://qa-test.avenuecode.com/");		
	}
	
	@Test
	public void ShouldFailToAccessWithInvalidUsername() {
		
		HomePage home = new HomePage(driver);
		assertThat(home.isOpen(),is(true));
		
		LogInPage login = home.logIn();
		assertThat(login.isOpen(), is(true));
		
		login.login("mvr.souza@outlook.com", "22jfF4MNHp");
		
		assertThat(login.getAlertMessage(), is("Signed in successfully."));
	}
	
	@Test
	public void ShouldPassToAccesWithValidUsername() {
		
		HomePage home = new HomePage(driver);
		assertThat(home.isOpen(),is(true));
		
		LogInPage login = home.logIn();
		assertThat(login.isOpen(), is(true));
		
		login.login("markitow.souza@gmail.com", "22jfF4MNHp");
		
		assertThat(login.getAlertMessage(), is("Signed in successfuly"));
	}
	
	@Test
	public void ShouldPassToValidateMyTaskButtonIsAwaysOnTheNavBar() {
		
		HomePage home = new HomePage(driver);
		assertThat(home.isOpen(),is(true));
		
		assertThat(home.getMyTasks(), is("My Tasks"));
	}
	
	@Test
	public void ShouldFailToValidateWellcomeMessage() {
		
		HomePage home = new HomePage(driver);
		assertThat(home.isOpen(),is(true));
		
		LogInPage login = home.logIn();
		assertThat(login.isOpen(), is(true));
		
		TasksPage tasks = login.login("markitow.souza@gmail.com", "22jfF4MNHp");
		
		assertThat(tasks.getWellcomeText().contains("this is your todo list for today:"), is(false));
	}
	
	@Test
	public void ShouldPassToValidateWellcomeMessage() {
		
		HomePage home = new HomePage(driver);
		assertThat(home.isOpen(),is(true));
		
		LogInPage login = home.logIn();
		assertThat(login.isOpen(), is(true));
		
		TasksPage tasks = login.login("markitow.souza@gmail.com", "22jfF4MNHp");
		
		assertThat(tasks.getWellcomeText().contains("'s ToDo List"), is(true));
	}
	
	@Test
	public void ShouldFailToAdda2CharacterTaskName() {
	
		HomePage home = new HomePage(driver);
		assertThat(home.isOpen(),is(true));
		
		LogInPage login = home.logIn();
		assertThat(login.isOpen(), is(true));
		
		TasksPage tasks = login.login("markitow.souza@gmail.com", "22jfF4MNHp");
		
		tasks.addNewTask("ab");
				
		//assertThat(tasks.get.contains("ab"), is(true));
	}
	
	@Test
	public void ShouldFailToAdda250plusCharacterTaskName() {
		
		HomePage home = new HomePage(driver);
		assertThat(home.isOpen(),is(true));
		
		LogInPage login = home.logIn();
		assertThat(login.isOpen(), is(true));
		
		TasksPage tasks = login.login("markitow.souza@gmail.com", "22jfF4MNHp");
		
		tasks.addNewTask("atestToValidateThatANewTaskNameShould'nHaveMoreThenTwoHundredFiftyCharactersAsHisNameThisIsAnImportantTestToMakeSureThatTheNameOfTheTaskIsReadableAndDon'tOvercomesTheLimitPassedByUserStoryThisTaskNameIsAFictionalExperienceWithMoreThen250CharactersWithLettersNumbersAndNoSpaceb");
		
		//assertThat(TasksPage.table_todolist(driver).findElements(By.className("task_body col-md-7")).contains("testToValidateThatANewTaskNameShould'nHaveMoreThenTwoHundredFiftyCharactersAsHisNameThisIsAnImportantTestToMakeSureThatTheNameOfTheTaskIsReadableAndDon'tOvercomesTheLimitPassedByUserStoryThisTaskNameIsAFictionalExperienceWithMoreThen250CharactersWithLettersNumbersAndNoSpace"), is(true));
	}
	
	@Test
	public void ShouldPassToAddAvalidNameCharacterTaskName() {
		
		HomePage home = new HomePage(driver);
		assertThat(home.isOpen(),is(true));
		
		LogInPage login = home.logIn();
		assertThat(login.isOpen(), is(true));
		
		TasksPage tasks = login.login("markitow.souza@gmail.com", "22jfF4MNHp");
		tasks.addNewTask("This is a Regular Task");
		
		//assertThat(TasksPage.table_todolist(driver).findElements(By.className("task_body col-md-7")).contains("This is a ToDo Test"), is(true));
	}
	
	public void ShouldFailToCreateEmptySubTask() {
	

		HomePage home = new HomePage(driver);
		assertThat(home.isOpen(),is(true));
		
		LogInPage login = home.logIn();
		assertThat(login.isOpen(), is(true));
		
		TasksPage tasks = login.login("markitow.souza@gmail.com", "22jfF4MNHp");
		tasks.addNewTask("");		
		
	}
	
	@After
    public void cleanUp(){
        driver.manage().deleteAllCookies();
    }
	
	@After
	public void TearDown(){
		driver.quit();
	}
	
}
